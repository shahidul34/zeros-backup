package router

import (
	"github.com/labstack/echo"
	apiV1 "mygolang/api/v1"
)

func Routes(e *echo.Echo) {
	apiV1Home := &apiV1.Home{}

	// Index Page
	e.GET("/", apiV1Home.Index)

	// Heath Check Page
	e.GET("/health", apiV1Home.Health)

	e.Static("/static", "public/assets")
}
