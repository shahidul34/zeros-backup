package v1

type BaseHelperInterface interface {
	init(input interface{}) error
	checkAuthority() error
	checkInput() error
	doPerform() (interface{}, error)
	onRollback() error
	Execute(input interface{}) (interface{}, error)
}

type BaseHelper struct {
}

func (bh *BaseHelper) init(input interface{}) error {
	return nil
}

func (bh *BaseHelper) checkAuthority() error {
	return nil
}

func (bh *BaseHelper) checkInput() error {
	return nil
}

func (bh *BaseHelper) doPerform() (interface{}, error) {
	return nil, nil
}

func (bh *BaseHelper) onRollback() error {
	return nil
}

func (bh *BaseHelper) Execute(input interface{}) (interface{}, error) {
	bh.init(input)

	err := bh.checkAuthority()
	if err != nil {
		return nil, err
	}

	err = bh.checkInput()
	if err != nil {
		return nil, err
	}

	output, err := bh.doPerform()
	if err != nil {
		err = bh.onRollback()
	} else {
		return output, nil
	}

	return nil, err
}
