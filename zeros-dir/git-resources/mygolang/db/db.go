package db

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"reflect"
	"sync"
	"mygolang/config"
	"mygolang/enum/status"
)

type DmManagerInterface interface {
	initConnection()
	InsertSingleDocument(collectionName string, document interface{}) (primitive.ObjectID, error)
	InsertMultipleDocument(collectionName string, documents []interface{}) ([]interface{}, error)
	FindOneByObjId(collectionName string, objId primitive.ObjectID, objType reflect.Type) interface{}
	FindOneByStrId(collectionName string, strId string, objType reflect.Type) interface{}
}

type dmManager struct {
	ctx context.Context
	db  *mongo.Database
}

// Implementing Singleton
var singletonDmManager *dmManager
var onceDmManager sync.Once

func GetDmManager() *dmManager {
	onceDmManager.Do(func() {
		log.Println("[INFO] Starting Initializing Singleton DB Manager")
		singletonDmManager = &dmManager{}
		singletonDmManager.initConnection()
	})
	return singletonDmManager
}

func (dm *dmManager) initConnection() {
	// Base context.
	ctx := context.Background()
	dm.ctx = ctx
	clientOpts := options.Client().ApplyURI(config.DatabseConnectionString)
	client, err := mongo.Connect(ctx, clientOpts)
	if err != nil {
		log.Println("[ERROR] DB Connection error:", err.Error())
		return
	}

	db := client.Database(config.DatabaseName)
	dm.db = db

	log.Println("[INFO] Initialized Singleton DB Manager")
}

func (dm *dmManager) InsertSingleDocument(collectionName string, document interface{}) (primitive.ObjectID, error) {
	coll := dm.db.Collection(collectionName)

	result, err := coll.InsertOne(dm.ctx, document)
	if err != nil {
		log.Println("[ERROR] Insert document:", err.Error())
		return primitive.ObjectID{}, err
	}

	// ID of the inserted document.
	objectID := result.InsertedID.(primitive.ObjectID)
	return objectID, nil
}

func (dm *dmManager) InsertMultipleDocument(collectionName string, documents []interface{}) ([]interface{}, error) {
	coll := dm.db.Collection(collectionName)
	results, err := coll.InsertMany(dm.ctx, documents)
	if err != nil {
		log.Println("[ERROR] Insert multiple documents:", err.Error())
		return nil, err
	}

	return results.InsertedIDs, nil
}

func (dm *dmManager) FindOneByObjId(collectionName string, objId primitive.ObjectID, objType reflect.Type) interface{} {
	coll := dm.db.Collection(collectionName)

	findResult := coll.FindOne(dm.ctx, bson.M{"_id": objId, "status": status.Valid})
	if err := findResult.Err(); err != nil {
		log.Println("[ERROR] Find document:", err.Error())
		return nil
	}

	objValue := reflect.New(objType)
	obj := objValue.Interface()
	err := findResult.Decode(obj)
	if err != nil {
		log.Println("[ERROR] Find document decoding:", err.Error())
		return nil
	}
	return obj
}

func (dm *dmManager) FindOneByStrId(collectionName string, strId string, objType reflect.Type) interface{} {
	objId, err := primitive.ObjectIDFromHex(strId)
	if err != nil {
		log.Println("[ERROR] Converting Str ID to Obj ID:", err.Error())
		return nil
	}
	return dm.FindOneByObjId(collectionName, objId, objType)
}