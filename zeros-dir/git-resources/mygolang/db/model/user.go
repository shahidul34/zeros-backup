package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
	"mygolang/enum"
)


type UserProfile struct {
	ID         primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	FirstName  string             `bson:"first_name" json:"firstName,omitempty"`
	LastName   string             `bson:"last_name" json:"lastName,omitempty"`
	Email      string             `bson:"email" json:"email,omitempty"`
	PhoneNo    string             `bson:"phone_no" json:"phoneNo,omitempty"`
	Status     enum.Status	  	  `bson:"status" json:"status,omitempty"`
	CreateDate time.Time          `bson:"create_date" json:"create_date,omitempty"`
	UpdateDate time.Time          `bson:"update_date" json:"update_date,omitempty"`
}

type User struct {
	ID          primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	Username    string             `bson:"username" json:"username,omitempty"`
	Password    string             `json:"-"`
	Authorities []enum.Authority   `bson:"authorities" json:"authorities,omitempty"`
	Profile     UserProfile        `bson:"profile" json:"profile,omitempty"`
	Status      enum.Status		   `bson:"status" json:"status,omitempty"`
	CreateDate  time.Time           `bson:"create_date" json:"create_date,omitempty"`
	UpdateDate  time.Time           `bson:"update_date" json:"update_date,omitempty"`
}
