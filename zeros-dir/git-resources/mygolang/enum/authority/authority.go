package authority

import "mygolang/enum"

const (
	RoleSuperAdmin   enum.Authority = "ROLE_SUPER_ADMIN"
	RoleAdmin        enum.Authority = "ROLE_ADMIN"
	RoleManager      enum.Authority = "ROLE_MANAGER"
	RoleExecutive    enum.Authority = "ROLE_EXECUTIVE"
	RoleCompliance   enum.Authority = "ROLE_COMPLIANCE"
	RolePartnerAdmin enum.Authority = "ROLE_PARTNER_ADMIN"
	RolePartnerUser  enum.Authority = "ROLE_PARTNER_USER"
	RoleUser         enum.Authority = "ROLE_USER"
	RoleAnonymous    enum.Authority = "ROLE_ANONYMOUS"
)
