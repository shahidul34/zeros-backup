package status

import "mygolang/enum"

const (
	Valid enum.Status = "V"
	Deleted enum.Status = "D"
)