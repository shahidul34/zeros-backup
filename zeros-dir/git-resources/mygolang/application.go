package main

import (
	"mygolang/config"
	"mygolang/router"
	"mygolang/server"
)

func main() {
	// Load Environment Variables
	config.InitEnvironmentVariables()

	srv := server.New()
	router.Routes(srv)
	srv.Logger.Fatal(srv.Start(":7070"))
}
