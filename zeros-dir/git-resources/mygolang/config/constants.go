package config

import _type "mygolang/type"

const (
	Develop    _type.RunMode = "DEVELOP"
	Production _type.RunMode = "PRODUCTION"
)
