package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	_type "mygolang/type"
)

var RunMode _type.RunMode

func InitEnvironmentVariables() {
	RunMode = _type.RunMode(os.Getenv("RUN_MODE"))
	if RunMode == "" {
		RunMode = Develop
		log.SetFlags(log.LstdFlags | log.Lshortfile)
	}

	log.Println("[INFO] RUN MODE:", RunMode)

	if RunMode != Production {
		err := godotenv.Load()
		if err != nil {
			log.Println("[ERROR] Loading environment variables from .env file:", err.Error())
			return
		}
	}
}
