package v1

import (
	"github.com/labstack/echo"
	"net/http"
)

type HomeInterface interface {
	Index(c echo.Context) error
	Health(c echo.Context) error
	PrivatePage(c echo.Context) error
}

type Home struct{}

func (h *Home) Index(c echo.Context) error {
	return c.Render(http.StatusOK, "index.html", map[string]interface{}{})
}

func (h *Home) Health(c echo.Context) error {
	return c.String(http.StatusOK, "I am live!")
}