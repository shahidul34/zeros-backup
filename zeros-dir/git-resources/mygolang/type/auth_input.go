package _type

type LoginInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type RefreshTokenRequestInput struct {
	RefreshToken string `json:"refreshToken"`
}
